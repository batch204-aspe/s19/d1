/*
	
	Selection Control Stuctures
		► sorts out whether the statements are to be executed based on the condition whether it's true or false


		☼ id else statement
		☼ switch stathe


		if... else statement
		Syntax: 

			if (condition){
					// statement - true
			} else {
					// statement - false
			}

*/ 


/*
	IF STATEMENT
		// executes a statement if a specified condition is true
		// can stand alone even without the else statement
	SYntax:
		if(condition){
			code block // True/False output statement
		}

*/

let numA = -1;

if (numA < 0 ){ // The statement is printed because the condition is met
	console.log("Hello");
}

if (numA > 0 ){  // if the condition is not met it will not be printed
	console.log("This statement will not be printed!");
}

// Another Example
let city = "New York";
if (city === "New York"){
	console.log("Welcome to New York City!");
}

// Else if
/*
	 Execute a statement if previous conditions are flase and if the specified condition is true

	 Else if lass is OPTIONAL and can be added to capture additional conditions to change the flow of a program

*/

let numB = 1;
if (numA > 0){
	console.log("Hello from elseif"); 
}
else if (numB > 0){
	console.log("World");
}

// Another Example
city = "Tokyo";

if (city === "New York"){
	console.log("Welcome to New York City!");
}
else if (city === "Tokyo"){
	console.log("Welcome to Tokyo City!");
}

//else statement
/*
	Executes a statement if all other conditions are false
	the "else" statement is optional and can be added to capture any Other result to change the flow of a proram

*/
if (numA > 0){
	console.log("hello");
}
else if (numB === 0){
	console.log("World");
}
else {
	console.log("Again!");
}

// Another Example
let age = 20;
if (age <= 18){
	console.log("Not Allowed to drink");
}
else{
	console.log("You're Allowed to drink, shot na!");
}

/*
	Mini Activity:
		- Create a conditional statement that if height is below 150, display "Did not passed the minimum requirement".

		- If above 150, display "Passed the minimum height requirement".
	
	Stretch Goal
		Put it inside a Function

*/
// Mini Activity Solution
let height = 150;

if (height <= 150 ){
	console.log("Did not passed the minimum requirement");
} else {
	console.log("Passed the minimum height requirement");
}

// Stretch Goal Solution
function heightReq(height){
	if (height <= 150 ){
		console.log("Did not passed the minimum requirement");
	} else {
		console.log("Passed the minimum height requirement");
	}
};
heightReq(150);

// if, else if and else statement with Function
let message = "No Message";
console.log(message);

function determineTyphoonIntensity(windSpeed) {

	if(windSpeed < 30) {
		return 'Not a typhoon yet.';
	}

	else if (windSpeed <= 61 ){
		return 'Tropical depression detected.';
	}

	else if (windSpeed >= 62 && windSpeed <= 88 ){
		return 'Tropical Storm Detected.';
	}

	else if (windSpeed >= 89 && windSpeed <= 117 ){
		return 'Severe Tropical Storm Detected.';
	}

	else {
		return 'Typhoon detected.';
	}
};
message = determineTyphoonIntensity(70);
console.log(message);

// Using Input of the user

/*

let windSpeed = prompt("Enter the wind Speed");
message = determineTyphoonIntensity();
console.log(message);


message = determineTyphoonIntensity(prompt("Wind Speed: "));
console.log(message); 

*/


if (message === 'Tropical Storm Detected.'){
	console.warn(message);
}

/*
	
	Truthy & Falsy

		- In JavaScript a "Truthy" value is a value that is considered true when encountered in a Boolean context

		- Values are considered true unless defined otherwise

		-falsy values/exceptions for truthy
			1. false
			2. 0
			3. -0
			4. ""
			5. null
			6. underlined
			7. Nan


*/ 

// Truthy Examples
if (true){
	console.log("Truthy!");
}
if(1){
	console.log("Truthy!");
}
if([]){
	console.log("Truthy!");
}

// Falsy Examples
if(false) {
	console.log("Falsy");
}
if(0) {
	console.log("Falsy");
}
if(-0) {
	console.log("Falsy");
}
if("") {
	console.log("Falsy");
}
if(undefined) {
	console.log("Falsy");
}

/*
	Conditional (Ternary ) Operator
		► The condition (ternary) operator takes in thress operands:
			1. Condition
			2. expression to execute if the condition is truthy
			3. expression to execute if the condition is falsy

		- Can be used as an alternative to an "if else statement"

		- Ternary Operators have an implicit "return" Statement meaning that wihout the  "return" keyword, the resulting expressions can be storred in a variable

		- it can be use in a one liner code but can be used in different operataions

		SYntax:
				(expression) ? ifTrue : ifFalse

*/

// Single Statement Exection
let ternaryResult = (1 < 18 ) ? true : false; // can also used different data type i.e String, Number...
console.log("Result of the Ternary Operator: " + ternaryResult);

// Multiple Statement Execution of Ternary Operator
let name = prompt("Enter Your Name");

function isOfLegalAge() {
	//name = 'John';
	return 'You are of the legal age limit';

};

function isUnderAge(){
	//name = "Jane";
	return 'You are under the age limit';
};

// The "parseInt" Function converts the input received into a number data type
let age2 = Number(prompt("What is your Age: "));
console.log(age2);

let legalAge = (age2 > 18) ? isOfLegalAge() : isUnderAge();
console.log("Result of the Ternary Operator in Function: " + legalAge + ', ' + name);

/*

	Switch Statement
		► Can be used as an alternative to an if.. else statement where the data used in the condition of an expected input

		Syntax:

			switch (expression){
				case <value>:
				statement;
				break;

				default:
				statement;
				break;

			}
*/
let day = prompt("What day of the week is it today?").toLowerCase();
console.log(day);

switch (day) {
	case 'monday', "lunes":
		console.log("The color of the day is red");
		break;
	case 'tuesday':
		console.log("The color of the day is orange");
		break;
	case 'wednesday':
		console.log("The color of the day is yellow");
		break;
	case 'thursday':
		console.log("The color of the day is green");
		break;
	case 'friday':
		console.log("The color of the day is blue");
		break;
	case 'saturday':
		console.log("The color of the day is indigo");
		break;
	case 'sunday':
		console.log("The color of the day is violet");
		break;
	default:
		console.log("Please Input a valid day!");
		break;
}

/*
	Try-Catch-Finally Statement
		► are commonly used for error handling
*/

function showIntensityAlert(windSpeed){

	try {
		//Attempt to execute a code
		alert(determineTyphoonIntensity(windSpeed));
	}
	// spots the error
	catch (error){
		console.warn(error);
	}
	finally {
		alert('Intensity updates will show new alert');
	}
};
showIntensityAlert(56);
